all: init convert send_files experiment

init:
	chmod +x setting_scripts.sh
	chmod +x prepare_scripts.sh
	./prepare_scripts.sh get_reproduction_data Capital 1
	./prepare_scripts.sh get_reproduction_data Capital 2
	./prepare_scripts.sh get_reproduction_data Divvy 1
	./prepare_scripts.sh get_reproduction_data Divvy 2
	./prepare_scripts.sh get_reproduction_data Hubway 1
	./prepare_scripts.sh get_reproduction_data Hubway 2

convert: 
	python convert_data.py 

send_files:
	./prepare_scripts.sh send_scripts
	./prepare_scripts.sh send_data Capital 1
	./prepare_scripts.sh send_data Capital 2
	./prepare_scripts.sh send_data Divvy 1
	./prepare_scripts.sh send_data Divvy 2
	./prepare_scripts.sh send_data Hubway 1
	./prepare_scripts.sh send_data Hubway 2

experiment:
	@echo -n "This process can be long, and you may want to launch independently proceed?[y/N]" && read ans && [ $${ans:-N} = y ]
	./setting_scripts.sh experiment Capital 1
	./setting_scripts.sh experiment Capital 2
	./setting_scripts.sh experiment Divvy 1
	./setting_scripts.sh experiment Divvy 2
	./setting_scripts.sh experimentHubway1 Hubway 1
	./setting_scripts.sh experiment Hubway 2

analysis:
	python prepare_analysis.py
	
cleanData:
	@echo -n "You will loose all the data [y/N]" && read ans && [ $${ans:-N} = y ]
	rm -rf Capital1/Demand1Capital_test1
	rm -rf Capital2/Demand2Capital_test1
	rm -rf Divvy1/Demand1Divvy_test1/
	rm -rf Divvy2/Demand2Divvy_test1/
	rm -rf Hubway1/Demand1Hubway_test1/
	rm -rf Hubway2/Demand2Hubway_test1/

cleangeojson:
	rm -rf Capital1/Demand1Capital_test1_geojson/
	rm -f Capital1/*.geojson
	rm -rf Capital2/Demand2Capital_test1_geojson/
	rm -f Capital2/*.geojson
	rm -rf Divvy1/Demand1Capital_test1_geojson/
	rm -f Divvy1/*.geojson
	rm -rf Divvy2/Demand2Divvy_test1_geojson/
	rm -f Divvy2/*.geojson
	rm -rf Hubway1/Demand1Capital_test1_geojson/
	rm -f Hubway1/*.geojson
	rm -rf Hubway2/Demand2Hubway_test1_geojson/
	rm -f Hubway2/*.geojson

cleanstarling:
	./prepare_scripts.sh cleanstarling Capital 1
	./prepare_scripts.sh cleanstarling Capital 2
	./prepare_scripts.sh cleanstarling Divvy 1
	./prepare_scripts.sh cleanstarling Divvy 2
	./prepare_scripts.sh cleanstarling Hubway 1
	./prepare_scripts.sh cleanstarling Hubway 2

cleanresults:
	rm -rf Capital1/Result_*/
	rm -rf Capital2/Result_*/
	rm -rf Divvy1/Result_*/
	rm -rf Divvy2/Result_*/
	rm -rf Hubway1/Result_*/
	rm -rf Hubway2/Result_*/

cleanAnalysis:
	rm -rf Capital1/Starling_results
	rm -rf Capital2/Starling_results
	rm -rf Divvy1/Starling_results
	rm -rf Divvy2/Starling_results
	rm -rf Hubway1/Starling_results
	rm -rf Hubway2/Starling_results

clean: cleanData cleangeojson cleanstarling cleanresults

#!/bin/bash
DATA_HOME=$(dirname -- "$0")
STARLING_HOME="$HOME/Documents/Tellae/starling"
NBSCEN=9

get_reproduction_data(){
    city="$1"
    istnb="$2"
    mkdir -p "$DATA_HOME/$city$istnb/"
    cp -R "$HOME/Documents/Datner_res/Results/$city$istnb/$city$istnb""_1/." "$DATA_HOME/$city$istnb/ReDatner_results"
    cp -r "$HOME/Documents/Datner_res/Data/$city$istnb/Demand$istnb$city""_test1/" "$DATA_HOME/$city$istnb/"
    cp -r "$HOME/Documents/Datner_res/Data/$city$istnb/Demand$istnb$city.mat" "$DATA_HOME/$city$istnb/"
    cp -r "$HOME/Documents/Datner_res/Data/$city$istnb/Geography$city.mat" "$DATA_HOME/$city$istnb/"
}

send_scripts(){
    cp "$DATA_HOME/setting_scripts.sh" "$STARLING_HOME"
    cp "$DATA_HOME/Capital1/GeographyCapital.mat" "$STARLING_HOME/data/environment/osm_graphs/"
    cp "$DATA_HOME/Hubway1/GeographyHubway.mat" "$STARLING_HOME/data/environment/osm_graphs/"
    cp "$DATA_HOME/Divvy1/GeographyDivvy.mat" "$STARLING_HOME/data/environment/osm_graphs/"
}

send_data(){
    city="$1"
    istnb="$2"
    istdir="$STARLING_HOME/data/models/SB_VS/$city$istnb"
    mkdir -p "$istdir"
    cp "$city$istnb/Geography$city""_half.geojson" "$istdir/"
    cp "$city$istnb/Geography$city""_RK.geojson" "$istdir/"
    cp "$city$istnb/Geography$city""_HO.geojson" "$istdir/"
    cp "$city$istnb/Geography$city""_HT.geojson" "$istdir/"
    cp "$city$istnb/Geography$city""_RKO.geojson" "$istdir/"
    cp "$city$istnb/Geography$city""_RKT.geojson" "$istdir/"
    for i in $(seq 0 $NBSCEN)
    do
        cp -r "$city$istnb/Demand$istnb$city""_test1_geojson/Demand$istnb$city""_$i" "$STARLING_HOME/data/models/SB_VS/$city$istnb/"
        ln -sf "$istdir/Geography$city""_half.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_half.geojson"
        ln -sf "$istdir/Geography$city""_RK.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_RK.geojson"
        ln -sf "$istdir/Geography$city""_HO.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_HO.geojson"
        ln -sf "$istdir/Geography$city""_HT.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_HT.geojson"
        ln -sf "$istdir/Geography$city""_RKO.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_RKO.geojson"
        ln -sf "$istdir/Geography$city""_RKT.geojson" "$istdir/Demand$istnb$city""_$i/inputs/Geography$city""_RKT.geojson"
    done
}

cleanstarling(){
    city="$1"
    istnb="$2"
    rm -rf "$STARLING_HOME/data/models/SB_VS/$city$istnb"
}
"$@"

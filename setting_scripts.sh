#!/bin/bash
DATA_HOME=$(dirname -- "$0")
STARLING_HOME="$HOME/Documents/Tellae/starling"
NBSCEN=9


launch_simu(){
    city="$1"
    istnb="$2"
    cd "$STARLING_HOME"
    for i in $(seq 0 $NBSCEN)
    do
        echo "Simulation of $city$istnb scenario $i"
        python main.py "$STARLING_HOME/data/models/SB_VS/$city$istnb/Demand$istnb$city""_$i/inputs/Params.json"
    done
    cd -
}

clean(){
    city="$1"
    istnb="$2"
    rm -r "$city$istnb/Demand$istnb$city""_test1_geojson/"
}


get_data(){
    city="$1"
    istnb="$2"
    repartition="$3"
    mkdir -p "$DATA_HOME/$city$istnb/Result""_$repartition/"
    cp -r $STARLING_HOME/data/models/SB_VS/$city$istnb/* "$DATA_HOME/$city$istnb/Result""_$repartition/"
}

update_param(){
    city="$1"
    istnb="$2"
    oldGeography="$3"
    newGeography="$4"
    for i in $(seq 0 $NBSCEN)
    do
        sed -i "s/Geography$city""_$oldGeography/Geography$city""_$newGeography/g" "$STARLING_HOME/data/models/SB_VS/$city$istnb/Demand$istnb$city""_$i/inputs/Params.json"
    done

}

experiment(){
    city="$1"
    istnb="$2"
    stocks=("half" "RK" "HT" "HO" "RKT" "RKO")
    for st in ${!stocks[@]}; do 
        echo $st
        launch_simu "$city" "$istnb"
        get_data "$city" "$istnb" "${stocks[$st]}"
        if (( $st < 5 ))
        then
            echo "update ${stocks[$st]} ${stocks[((st+1))]}"
            update_param "$city" "$istnb" "${stocks[$st]}" "${stocks[((st+1))]}"
        fi
    done
}


experimentHubway1(){
    city="Hubway"
    istnb="1"
    stocks=("half" "HT" "HO") 
    for st in ${!stocks[@]}; do 
        launch_simu "$city" "$istnb"
        get_data "$city" "$istnb" "${stocks[$st]}"
        if (( $st < 2 ))
        then
            update_param "$city" "$istnb" "${stocks[$st]}" "${stocks[((st+1))]}"
        fi
    done
}

"$@"

# Presentation

This Repository is made to replicate the findings of ReDatner into the starling simulator.
In order to do this you will need to install the starling simulator and the data from the ReDatner experiment.
If you don't understand a word of what it is written you maybe want to check my thesis and/or the ReDatner experiment [LINKTODO]
The paragraphs of this README should be executed in the correct order so please read.

# Installation
If you are reading this you probably have cloned the repository, if it is not already made please do so.
You must also download the starling version that you can find in [LINKTODO] and install it.
I choose to organise my directories as follows, you are free to differ and it should work but it is untested on other organization:

- data-preparation |
    * data-preparation
- starling |
    * starling main repertory

Once the starling simulator installed, please ensure that you have installed the python environement that would make starling work, we will always use this environment in the scripts.
Then ensure that the variables in `prepare_scripts.sh` and `setting_scripts.sh` are correctly set
`STARLING_HOME` should refer to the main directory of starling, `NBSCEN` correspond to the number of scenarios that you want to test. `DATA_HOME` should refer to the root directory of the project (you should'nt have to change it).

# Organisation
There is a Makefile aimed to help in the automation of the processing and the simulation.
Some scripts are made in bash in order to do the commands, and some process are made in python. The organisation is quite fixed to help in the automation of the processes, if you use the makefile it will follow this organisation, if not please ensure this organisation is respected.
We will create a directory for each instance, the directories will be named and organized as follows:
```
./{city}{instanceNumber}/ |
   ReDatner_results/
   geography{city}.mat 
   Demand{instanceNumber}{city}.mat 
   Demand{instanceNumber}{city}_test1/ |
        Demand{instanceNumber}{city}_{scenarioNumber}.csv 
```

# Getting the data
You can get all the data needed by using
```
make init
```
It will also organize correctly the data for the future conversion. If the links are dead or not updated refer to the previous paragraph.

# Preparing the data
The starling simulator follows a particular organisation in order to run.
You must start by converting the data in the correct format by using 

```
make convert
```

Once the conversion is made you can send the data to the starling simulator in order to prepare the run
```
make send_files
```

# Experiment
If all the previously made worked properly you can now use to launch all the experiments
```
make experiment
```

Be aware that this task can be long, if you want to launch an unique experiment you can use 
```
./setting_scripts.sh experiment {city} {instanceNumber}
```
or to launch the experiment Hubway 1 (which misses data)

```
./setting_scripts.sh experimentHubway1
```

Once the process is finished you will find the output the results in `{city}{instanceNumber}/Result_{initialStock}`

WARNING: If you want to relaunch the experiments you must resend the data previously to launch the experiment.

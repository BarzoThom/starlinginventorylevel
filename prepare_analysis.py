import os

import scipy.io as sio
import pandas as pd

def prepare_excessTime(Instance):
    city = Instance[:-1]
    instanceNb = Instance[-1]
    demand_pattern = f"Demand{instanceNb}{city}"
    RidingTime = sio.loadmat(f"{Instance}/Geography{city}.mat")["RidingTime"]
    results = []
    for scenario_nb in range(10):
        users = pd.read_csv(f"{Instance}/{demand_pattern}_test1/{demand_pattern}_{scenario_nb}.csv")
        users[["startStation", "endStation"]]=users[["startStation", "endStation"]].astype(int)
        users["scenario.number"] = scenario_nb
        users["idealTime"] = [RidingTime[x, y] for x,y in zip(users.startStation, users.endStation)]
        users.loc[users["idealTime"] == 0, "idealTime"] = 1
        users["agentId"] = "u_" + users.index.astype(str)
        results.append(users)
    pd.concat(results, axis=0).to_csv(f"{Instance}/IdealTime.csv", index=False)

def prepare_result(Instance, result_type):
    city = Instance[:-1]
    instanceNb = Instance[-1]
    demand_pattern = f"Demand{instanceNb}{city}"
    nbSS = []
    nbSS_nir = []
    nbIR = []
    IRr = []
    ET = []
    ideal_time = pd.read_csv(f"{Instance}/IdealTime.csv")
    ideal_time["starttime"] *= 30*60
    ideal_time["idealTime"] *= 30*60
    for scenario_nb in range(10):
        ukpi = pd.read_csv(f"{Instance}/Result_{result_type}/{demand_pattern}_{scenario_nb}/outputs/user_kpi.csv.gz", sep=";")
        ukpi["scenario.number"] = scenario_nb
        ukpi = ukpi.merge(ideal_time, on=["scenario.number", "agentId"], how="left")
        nbSS.append(sum(ukpi.nbFailedRequest))
        nbSS_nir.append(ukpi.loc[ukpi.walkTime > 0, "nbFailedRequest"].mean())
        nbIR.append(len(ukpi[ukpi.walkTime == 0]))
        IRr.append((ukpi.walkTime == 0).mean())
        ET.append(sum(ukpi.destinationReachedTime - ukpi.starttime - ukpi.idealTime))
    result = pd.DataFrame({"scenario.number":range(10), "Number.of.shortage.and.surplus.events":nbSS,
                           "Avg.number.of.shortage.and.surplus.per.nonideal.ride.user":nbSS_nir,
                           "Number.of.ideal.rides":nbIR, "Ideal.ride.ratio":IRr, "Excess.time":ET})
    result.to_csv(f"{Instance}/Starling_results/Result_{result_type}.csv",index=False)

def prepare_analysis(Instance):
    prepare_excessTime(Instance)
    os.makedirs(f"{Instance}/Starling_results", exist_ok=True)
    if Instance == "Hubway1":
        resultTypes = ["half", "HO", "HT"]
    else:
        resultTypes = ["half", "HO", "HT", "RK", "RKO", "RKT"]
    for rtype in resultTypes:
        prepare_result(Instance, rtype)

prepare_analysis("Capital1")
prepare_analysis("Capital2")
prepare_analysis("Divvy1")
prepare_analysis("Divvy2")
prepare_analysis("Hubway1")
prepare_analysis("Hubway2")

"""
Module to convert Data provided by Tal Raviv, into data simulable by STARLING
"""

import os
import csv
from geojson import Feature, FeatureCollection, LineString, Point, dump
import glob

import numpy as np
import scipy.io as sio


def make_stations_half(geography):
    List_stations = []
    for ist, st in enumerate(geography["C"]):
        pt = Point((0, 0))
        prp = {"agent_id": f"st_{ist}",
               "agent_type": "station",
               "capacity": int(st[0]),
               "icon": "station",
               "mode": "bike",
               "origin": ist,
               "stock_generation": {"generated_stock":int(st[0]/2)}}
        List_stations.append(Feature(geometry=pt, properties=prp))
    return FeatureCollection(List_stations)

def make_stations_stock(geography, stock):
    List_stations = []
    for ist, (st, inis) in enumerate(zip(geography["C"], stock)):
        pt = Point((0, 0))
        prp = {"agent_id": f"st_{ist}",
               "agent_type": "station",
               "capacity": int(st[0]),
               "icon": "station",
               "mode": "bike",
               "origin": ist,
               "stock_generation": {"generated_stock":int(inis)}}
        List_stations.append(Feature(geometry=pt, properties=prp))
    return FeatureCollection(List_stations)


def make_demand(scenario_file, simu_time=[0,48]):
    with open(scenario_file) as sc_file:
        reader = csv.reader(sc_file)
        next(reader)
        List_users=[]
        for irow, row in enumerate(reader):
            starttime = int(30*60*float(row[0]))
            if float(row[0]) < simu_time[0]:
                continue
            if float(row[0]) > simu_time[1]:
                break
            startStation = int(row[1])
            endStation = int(row[2])
            line = LineString([(startStation, startStation), (endStation, endStation)])
            line = Point((0,0))
            prp = {"agent_id": f"u_{irow}",
                   "agent_type": "user",
                   'icon': 'user',
                   "origin_station": f"st_{startStation}",
                   "destination_station": f"st_{endStation}",
                   "origin_time": starttime,
                   "has_station_info": True,
                   "patience": 5,
                   "mode": "walk"}
            List_users.append(Feature(geometry=line, properties=prp))
        return FeatureCollection(List_users)

def convert_directory(Instance, demand_pattern, simu_time=[0,48]):
    """
    Convert the 10 first scenarios of the directory to starling format, and add those into new directory (created if not present)
    @param Instance (path-like): The directory where to find the files of the instances
    @param demand_pattern (str): base for demand file, should correspond to architecture described in README.md
    @param simu_time (tuple): time limit for simulation in half an hour (default to [0,48])
    """
    indir = os.path.join(Instance, demand_pattern+"_test1")
    outdir = indir+"_geojson"
    os.makedirs(outdir, exist_ok=True)
    scenarios = [os.path.join(indir,f"{demand_pattern}_{i}.csv") for i in range(10)]
    for iscen, scenario_file in enumerate(scenarios):
        print(f"Conversion of scenario {iscen+1}/{len(scenarios)}", end="\r")
        scen_dir = os.path.join(outdir, f"{demand_pattern}_{iscen}", "inputs")
        os.makedirs(scen_dir, exist_ok=True)
        users = make_demand(scenario_file, simu_time)
        userfile = os.path.join(scen_dir,f"{demand_pattern}_{iscen}.geojson")
        with open(userfile, "w+") as uf:
            dump(users, uf)

def generate_params(demand_pattern, env_file, Instance):
    """Generate Params.json for the 10 first scenario of the instance
    @param demand_pattern (str): Format of the demand file see README.md
    @param env_file (path-like): Path to the geography file of the instance
    @param Instance (str): Name of the instance in format {city}{instance_number}
    """
    dirname = f"{demand_pattern}_test1_geojson"
    city = Instance[:-1]
    scenario_directories = [os.path.join(Instance, dirname, f"{demand_pattern}_{i}") for i in range(10)]
    for iscen, scenario_dir in enumerate(scenario_directories):
        if not os.path.isdir(scenario_dir):
            raise ValueError(f"{scenario_dir} not found convert demand first")
        param = {"code":"SB_VS",
                 "scenario": os.path.join(Instance,f"{demand_pattern}_{iscen}"),
                 "limit": 10000000,
                 "seed":42,
                 "date":"2022-03-01",
                 "traces_output": False,
                 "visualisation_output": False,
                 "kpi_output": True,
                 "dynamic_input_file":f"{demand_pattern}_{iscen}.geojson",
                 "init_input_file": env_file,
                 "topologies": {
                     "walk": [f"Geography{city}.mat", "speeds_walk.json"],
                     "bike": [f"Geography{city}.mat", "speeds_bike.json"]}
                 }
        outdir = os.path.join(scenario_dir, "inputs")
        with open(os.path.join(outdir, "Params.json"), "w+") as of:
            dump(param, of)



def convert_geography_half(geography_file):
    """
    Will place create the geojson file for the half repartition, be aware than the created file must be moved into the demand directory and the links must be created.
    see README.md for more information
    @param geography_file (path-like): the geography file to convert
    """
    geography = sio.loadmat(geography_file)
    stations = make_stations_half(geography)
    envfile = os.path.splitext(geography_file)[0] + "_half.geojson"
    with open(envfile, "w+") as ef:
        dump(stations, ef)

def convert_geography_RK(geography_file, demand_file):
    """
    Will create the geojson for the R&K initial repartition
    @param geography_file (path-like): the geography file to convert
    @param demand_file (path-like): the demand file to find the repartition
    """
    geography = sio.loadmat(geography_file)
    stock = sio.loadmat(demand_file)["InitialState00ofer"]
    stations = make_stations_stock(geography, stock)
    envfile = os.path.splitext(geography_file)[0] + "_RK.geojson"
    with open(envfile, "w+") as ef:
        dump(stations, ef)

def convert_geography_opti(geography_file, stockfile, stocktype):
    """
    Will create the geojson for the optimised repartition
    @param geography_file (path-like): the geography file to convert
    @param stockfile (path-like): the stock file to find the repartition
    @param stocktype (str): the type of optimisation made, will be append as a suffix of the geojson file.
    """
    geography = sio.loadmat(geography_file)
    stock = np.load(stockfile)
    stations = make_stations_stock(geography, stock)
    envfile = os.path.splitext(geography_file)[0] + f"_{stocktype}.geojson"
    with open(envfile, "w") as ef:
        dump(stations, ef)

def convert_instance(Instance):
    """
    Will convert a full instance to starling format, it will convert the 10 first scenario of test1 of the instance along with the 6 initial repartitions
    @param Instance (path-like): The directory where to find the files of the instances
    @param initial_repartition (str): the initial repartition to choose between (H for half,)
    """

    instance_nb = Instance[-1]
    city = Instance[:-1]
    demand_pattern = f"Demand{instance_nb}{city}"
    geography_file = f"{Instance}/Geography{city}.mat"
    print(f"Starting Demand Conversion of instance {Instance}")
    convert_directory(Instance, demand_pattern, [14,33])
    print('Geography Half')
    convert_geography_half(geography_file)
    print('Geography Half_O')
    convert_geography_opti(geography_file, f"{Instance}/ReDatner_results/stockOpti_half_14-33_O.npy", "HO")
    print('Geography Half_T')
    convert_geography_opti(geography_file, f"{Instance}/ReDatner_results/stockOpti_half_14-33_T.npy", "HT")
    if Instance != "Hubway1":
        print('Geography R&K')
        convert_geography_RK(geography_file, f"{Instance}/{demand_pattern}.mat")
        print('Geography R&K_O')
        convert_geography_opti(geography_file, f"{Instance}/ReDatner_results/stockOpti_RK_14-33_O.npy", "RKO")
        print('Geography R&K_T')
        convert_geography_opti(geography_file, f"{Instance}/ReDatner_results/stockOpti_RK_14-33_T.npy", "RKT")
    print("Generating params for simulation with half")
    generate_params(demand_pattern, f"Geography{city}_half.geojson", Instance)

convert_instance("Capital1")
convert_instance("Capital2")
convert_instance("Divvy1")
convert_instance("Divvy2")
convert_instance("Hubway1")
convert_instance("Hubway2")
